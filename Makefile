CC := gcc --std=c99
CFLAGS += -Wall -Wextra -Werror -pedantic -pedantic-errors
CFLAGS += -O2
CPPFLAGS += -D_POSIX_C_SOURCE=200809L
LDFLAGS += -Wl,--as-needed

CMOD ?= cmod

# build
all: eca1d

OBJS += binstring.o ccca.o

eca1d: $(OBJS)
eca1d: CFLAGS += -fopenmp
eca1d: LDLIBS += -lgomp
eca1d: CFLAGS += `gsl-config --cflags`
eca1d: LDLIBS += `gsl-config --libs`

binstring.o: binstring.h
binstring.o: CFLAGS += -DLIBRARY=1

ccca.o: ccca.h

clean:
	$(RM) eca1d $(OBJS) $(OBJS:.o=.c) $(OBJS:.o=.h) *~

# do stuff
RULE ?= 90
WIDTH ?= 1920
HEIGHT ?= 1080
SEED ?= $(shell date +%s)
N ?= 1000

ECA1D_FLAGS += -1 64

sampler:
	seq 0 255 | parallel $(MAKE) sample RULE={}

sample: R$(RULE)_W$(WIDTH)_H$(HEIGHT)_S$(SEED).png
R$(RULE)_W$(WIDTH)_H$(HEIGHT)_S$(SEED).png:
	for x in r g b; do \
		./eca1d -r $(RULE) -i random -w $(WIDTH) -h $(HEIGHT) -c --seed $(SEED) --random8 -C $@_$${x} $(ECA1D_FLAGS) -o /dev/null >/dev/null; \
		sleep 1; \
	done
	convert -size $(WIDTH)x$(HEIGHT) -depth 8 gray:$@_r gray:$@_g gray:$@_b -channel RGB -combine $@
	$(RM) $@_r $@_g $@_b

benchmark: R$(RULE)_W$(WIDTH)_H$(HEIGHT)_N$(N).txt
R$(RULE)_W$(WIDTH)_H$(HEIGHT)_N$(N).txt:
	seq 1 $(N) \
		| awk '{ print $(SEED) + $$1 }' \
		| parallel ./eca1d -r $(RULE) -i random --seed {} -c --no-write -w $(WIDTH) -h $(HEIGHT) \
		| awk '{ s[$$1]++ } END { for(x in s) print x,s[x] }' \
		| sort -k1g \
		> $@

# generic C% rules
%.c: %.cm
	$(CMOD) $(CMODFLAGS) -o $@ $<

%.h: %.hm
	$(CMOD) $(CMODFLAGS) -o $@ $<
