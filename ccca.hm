#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>

%include [sys] "cmod/autoarr.hm"

%prefix ccca;

%table selbits (bit) %{
	bit
	tmp
%}

/* selection bits */
%map selbits %{
#define SEL_$U{bit} (1UL << ${_NR})
%}

/* unit increments in X,Y,Z directions */
%table increments (name,var,incr) %{
	Jm1		j	- 1
	Jp1		j	+ 1
	Im1		i	- 1
	Ip1		i	+ 1
	Km1		k	- 1
	Kp1		k	+ 1
%}

%@(1)map [add1=%table-nrow (`selbits`)] increments %{
#define ${name} ${_NR} // ${var} ${incr}
#define NN_${name} (1UL << ${name}) // bit mask
%}

/* convenience bit masks */
#define NN_ALL   (NN_Jm1 | NN_Jp1 | NN_Im1 | NN_Ip1 | NN_Km1 | NN_Kp1) // = 252
#define NN_JMASK (NN_Im1 | NN_Ip1 | NN_Km1 | NN_Kp1) // = 240
#define NN_IMASK (NN_Jm1 | NN_Jp1 | NN_Km1 | NN_Kp1) // = 204
#define NN_KMASK (NN_Jm1 | NN_Jp1 | NN_Im1 | NN_Ip1) // = 60

#define ccca_u8(p,ix) (((uint8_t*)(p))[ix])

/* bit manipulators */
%map selbits %{
#define ccca_sel${bit}_set(u8p,ix)    (ccca_u8(u8p,ix) |= SEL_$U{bit})
#define ccca_sel${bit}_check(u8p,ix) ((ccca_u8(u8p,ix) >> ${_NR}) & 1UL)
#define ccca_sel${bit}_clear(u8p,ix)  (ccca_u8(u8p,ix) &= ~SEL_$U{bit})
#define ccca_sel${bit}_toggle(u8p,ix) (ccca_u8(u8p,ix) ^= SEL_$U{bit})
#define ccca_sel${bit}_copy(dst_u8p,dst_ix,src_u8p,src_ix)  (ccca_u8(dst_u8p,dst_ix) \
         ^= (-(unsigned)ccca_sel${bit}_check(src_u8p,src_ix) ^ ccca_u8(dst_u8p,dst_ix)) & SEL_$U{bit})
%}

#define ccca_nhood_get(u8p,ix)      ccca_u8 (u8p,ix)
#define ccca_nhood_set(u8p,ix,n)   (ccca_u8 (u8p,ix) |=  (n))
#define ccca_nhood_clear(u8p,ix,n) (ccca_u8 (u8p,ix) &= ~(n))

#define ccca_nhood_check(n,nn) (((nn) >> (n)) & 1UL)
#define ccca_nhood_mask(p) ((p) == PLANE_XY ? NN_KMASK : ((p) == PLANE_XZ ? NN_IMASK : NN_JMASK))

/* clear SEL_TMP and clear SEL_BIT */
#define ccca_sel_bctc(u8p,ix) (ccca_u8(u8p,ix) &= ~(SEL_TMP | SEL_BIT))
/* clear SEL_TMP and set SEL_BIT */
#define ccca_sel_bstc(u8p,ix) (ccca_u8(u8p,ix) = ((ccca_u8(u8p,ix) & ~SEL_TMP) | SEL_BIT))
/* set SEL_TMP and clear SEL_BIT */
#define ccca_sel_bcts(u8p,ix) (ccca_u8(u8p,ix) = ((ccca_u8(u8p,ix) & ~SEL_BIT) | SEL_TMP))

%snippet arr_ix:free_item (x) %{ %}
%snippet arr_arr_ix:free_item (x) %{ ${x} = ccca_arr_ix_free(${x}); %}

%recall std:autoarr_typedef (`arr_ix`,`size_t`,`128`)
%recall std:autoarr_typedef (`arr_arr_ix`,`ccca_arr_ix`,`16`)

%proto void setup_nhood(uint8_t x[static 1], size_t nx, size_t ny, size_t nz, bool three_d, bool parallel);
%proto void setup_nhood_2d(uint8_t x[static 1], size_t nx, size_t ny, bool parallel);
%proto struct ccca_arr_ix select_cc2d_u8(uint8_t sel[static 1], const uint8_t dat[static 1], size_t nx, size_t ny, size_t cx);
